package example.rotlite.rotlite;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabaseCorruptException;
import android.database.sqlite.SQLiteException;
import android.net.ConnectivityManager;
import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by claudio on 08/08/15.
 */
public class RotLiteObject implements RotLiteInterface, Serializable {

    public String name, className;
    ContentValues content;
    String TAG = "RotLiteObject";
    String TAG_ACTIVITY = "RotLiteObjectActivity";
    private List<String> columnTypes = new ArrayList<>();
    private Context context;

    private final OkHttpClient client = new OkHttpClient();
    private final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    private RotLiteCore dbcore;
    private SQLiteDatabase db;
    private long _id = 0;
    public Class obj;
    private String where = "1 = 1";
    private int limitMin = 0;
    private int limitMax = 0;
    private String limit = "";
    private String query = "";
    private String order = "";
    private String url = "";
    private String endpoint = "";
    private int method;
    private boolean activityLogs = false;
    private boolean autosync = false;
    private int from;
    private Map<String, String> dataType  = new HashMap<String, String>();
    public static String MODEL_TAG = "model";
    private boolean customWebService = false;

    public RotLiteObject() { }

    public RotLiteObject(Context context, Class obj) {
        this.context = context;
        dbcore = new RotLiteCore(context);
        url = RotLiteCore.getMeta(this.context, "rotlite_server");
        db = dbcore.getWritableDatabase();
        this.obj = obj;

        className = this.obj.getName();

        if (this.obj.isAnnotationPresent(Table.class)) {

            Annotation annotation = this.obj.getAnnotation(Table.class);
            Table table = (Table) annotation;

            this.name = table.name();
            this.endpoint = table.endpoint();
            this.autosync = table.autosync();

            this.url = this.url + this.endpoint;

            MODEL_TAG = this.name;

            if (!this.endpoint.equals("")) {
                this.customWebService = true;
            }

        }

        TAG = this.obj.getSimpleName();

        this.from = RotLiteConsts.FROM_LOCAL;

    }

    @Override
    public void setTbName(String name) {
        this.name = name;
    }

    @Override
    public String getTbName() {
        return name;
    }

    @Override
    public void setId(int id) {
        this._id = id;
    }

    @Override
    public long getId() {
        return this._id;
    }

    @Override
    public void put(String key, String value) {
        if (content == null) content = new ContentValues();
        content.put(key, value);
        dataType.put(key, RotLiteConsts.DATA_TYPE_STRING);
    }

    @Override
    public void put(String key, double value) {
        if (content == null) content = new ContentValues();
        content.put(key, value);
        dataType.put(key, RotLiteConsts.DATA_TYPE_DOUBLE);
    }

    @Override
    public void put(String key, int value) {
        if (content == null) content = new ContentValues();
        content.put(key, value);
        dataType.put(key, RotLiteConsts.DATA_TYPE_INTEGER);
    }

    @Override
    public void put(String key, long value) {
        if (content == null) content = new ContentValues();
        content.put(key, value);
        dataType.put(key, RotLiteConsts.DATA_TYPE_LONG);
    }

    @Override
    public void put(String key, boolean value) {
        if (content == null) content = new ContentValues();
        content.put(key, value);
        dataType.put(key, RotLiteConsts.DATA_TYPE_BOOLEAN);
    }

    @Override
    public String getString(String key) {
        return content.getAsString(key);
    }

    @Override
    public double getDouble(String key) {
        return content.getAsDouble(key);
    }

    @Override
    public int getInt(String key) {
        return content.getAsInteger(key);
    }

    @Override
    public long getLong(String key) {
        return content.getAsLong(key);
    }

    @Override
    public boolean getBoolean(String key) {
        return content.getAsBoolean(key);
    }

    @Override
    public void saveLocal() throws Exception {

        method = RotLiteConsts.METHOD_INSERT;

        try {
            putLastUpdate();
            _id = db.insertOrThrow(name, null, content);
            if (activityLogs) Log.v(TAG_ACTIVITY, "Data inserted into the table '" + name + "', id: " + _id + ", data: " + jsonString());
        }catch(SQLiteDatabaseCorruptException e) {
            Log.e(TAG, "SQLiteDatabaseCorruptException Code: " + e.hashCode() + "; Message: " + e.getMessage().toString());
            e.printStackTrace();
        }catch (android.database.sqlite.SQLiteConstraintException e) {
            Log.e(TAG, "SQLiteConstraintException " + e.getMessage());
        }catch (android.database.sqlite.SQLiteException e) {
            Log.e(TAG, "SQLiteException " + e.getMessage());
            String error = e.getMessage();

            if (error.contains(RotLiteConsts.ROTLITE_ERROR_NO_SUCH_TABLE)) {

                if (createTable(name)) {
                    saveLocal();
                }else{
                    throw new SQLiteException("Não foi possível criar a tabela '" + name + "'");
                }

            }else if (error.contains(RotLiteConsts.ROTLITE_ERROR_HAS_NO_COLUMN)) {

                String getColumn = error.substring(error.indexOf(RotLiteConsts.ROTLITE_ERROR_HAS_NO_COLUMN)
                        + RotLiteConsts.ROTLITE_ERROR_HAS_NO_COLUMN.length() + 1, error.length());
                getColumn = getColumn.substring(0, getColumn.indexOf(" "));

                Log.e(TAG, "Não tem a coluna '" + getColumn + "'");

                db.execSQL("ALTER TABLE " + name + " ADD COLUMN " + getColumn + "");
                saveLocal();

            }
        }
        catch (Exception e) {
            Log.e(TAG, "Exception " + e.getMessage());
        }
        //content = null;

    }

    @Override
    public void saveWeb(Callback callback) throws Exception {

        saveData(url, jsonObject(), callback);

    }

    @Override
    public void save() throws Exception {

        saveLocal();

        /**
         * Agora salvamos os dados numa tabela temporária, essa tabela possuirá
         * todos os dados que ainda não foram enviados para a nuvem.
         */

        final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        final Date date = new Date();

        final RotLiteModel temp = new RotLiteModel(context);
        temp.showActivityLogs(true);
        temp.where("model_name = '" + name + "' and data_id = " + _id + "");
        temp.find(new RotLiteCallback<List<RotLiteObject>>() {
            @Override
            public void done(List<RotLiteObject> list) {

                if (list.size() == 0) {
                    temp.put("model_name", name);
                    temp.put("data_id", _id);
                    temp.put("last_update", dateFormat.format(date));
                    try {
                        temp.saveLocal();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void error(RotLiteException e) {
                Log.e(TAG_ACTIVITY, e.getMessage());
            }
        });

        saveWeb(new Callback() {

            @Override
            public void onFailure(Request request, IOException e) {
                Log.e(TAG, "Failure request: " + e.getMessage());
            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (response.isSuccessful()) {
                    String responseStr = response.body().string();

                    Log.v(TAG, "Response: " + responseStr);

                    try {

                        JsonParser jsonParser = new JsonParser();
                        final JsonObject res = ((JsonObject) jsonParser.parse(responseStr)).getAsJsonObject();
                        JsonElement status = res.get("status");
                        //JsonElement message = res.get("message");

                        if (status.getAsString().equals("SUCCESS")) {

                            temp.find(new RotLiteCallback<List<RotLiteObject>>() {
                                @Override
                                public void done(List<RotLiteObject> list) {

                                    Log.v(TAG_ACTIVITY, "Temp size: " + list.size());
                                    for (RotLiteObject obj : list) {

                                        Log.v(TAG_ACTIVITY, "Temp: " + obj.jsonString());

                                    }

                                }

                                @Override
                                public void error(RotLiteException e) {
                                    Log.e(TAG_ACTIVITY, e.getMessage());
                                }
                            });
                            temp.delete();
                            temp.find(new RotLiteCallback<List<RotLiteObject>>() {
                                @Override
                                public void done(List<RotLiteObject> list) {

                                    Log.v(TAG_ACTIVITY, "Temp size 2: " + list.size());

                                }

                                @Override
                                public void error(RotLiteException e) {
                                    Log.e(TAG_ACTIVITY, e.getMessage());
                                }
                            });

                        } else if (status.getAsString().equals("ERROR")) {

                            JsonElement message = res.get("message");
                            throw new Exception("Server error: " + message.getAsString());

                        } else {

                            throw new Exception("Undefined server error");

                        }

                    } catch (Exception e) {
                        Log.e(TAG, e.getMessage());
                        e.printStackTrace();
                    }

                } else {
                    Log.e(TAG, "Response error: " + response.message());
                }
            }

        });

    }

    @Override
    public boolean tbExists(String tbname) {
        if(db == null || !db.isOpen()) {
            db = dbcore.getReadableDatabase();
        }

        if(!db.isReadOnly()) {
            db.close();
            db = dbcore.getReadableDatabase();
        }

        Cursor cursor = db.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '"+tbname+"'", null);
        if(cursor != null) {
            if(cursor.getCount()>0) {
                cursor.close();
                return true;
            }
            cursor.close();
        }
        return false;
    }

    @Override
    public boolean createTable(String tbname) {

        Set<String> keys = content.keySet();

        String keyString = "_id INTEGER PRIMARY KEY AUTOINCREMENT, rotlite_cloud_id INTEGER, rotlite_last_update DATETIME,";

        for (String key : keys) {

            try {

                String type = dataType.get(key);

                if (type.equals(RotLiteConsts.DATA_TYPE_STRING)) {
                    keyString = keyString + " " + key + " TEXT,";
                }else if (type.equals(RotLiteConsts.DATA_TYPE_INTEGER)) {
                    keyString = keyString + " " + key + " INTEGER,";
                }else if (type.equals(RotLiteConsts.DATA_TYPE_DOUBLE)) {
                    keyString = keyString + " " + key + " DOUBLE,";
                }else if (type.equals(RotLiteConsts.DATA_TYPE_LONG)) {
                    keyString = keyString + " " + key + " INTEGER,";
                }else if (type.equals(RotLiteConsts.DATA_TYPE_BOOLEAN)) {
                    keyString = keyString + " " + key + " TINYINT,";
                }

            }catch(Exception e) {
                Log.e(TAG, "Get value error: " + e.getMessage());
            }

        }

        keyString = "CREATE TABLE " + tbname + " (" + keyString.substring(0, keyString.length()-1) + ")";

        db.execSQL(keyString);

        if (tbExists(tbname)) {
            Log.v(TAG, "Created table: " + keyString);
            return true;
        }else{
            Log.v(TAG, "Error on create table " + name);
            return false;
        }

    }

    @Override
    public void where(String where) {
        this.where = where;
    }

    @Override
    public void find(final RotLiteCallback callback) {

        final List list = new ArrayList<>();

        if (this.from == RotLiteConsts.FROM_LOCAL) {

            String sql = "SELECT * FROM " + name + " WHERE " + where;

            if (!order.equals("")) {
                sql = sql + order;
            }

            if (!limit.equals("")) {
                sql = sql + limit;
            }

            query = sql;

            try {
                Cursor cursor = db.rawQuery(query, null);
                if (cursor.moveToFirst()) {

                    do {

                        String[] cols = cursor.getColumnNames();

                        RotLiteObject obj = new RotLiteObject();
                        obj.setTbName(name);

                        for (int i = 0; i < cols.length; i++) {

                            obj.put(cols[i], cursor.getString(i));

                        }

                        list.add(obj);

                    } while (cursor.moveToNext());

                }

                if (cursor != null && !cursor.isClosed()) {
                    cursor.close();
                }

                callback.done(list);

            } catch (SQLiteDatabaseCorruptException e) {
                callback.error(new RotLiteException("SQLiteDatabaseCorruptException " + e.getMessage().toString() + "", e.hashCode()));
            } catch (android.database.sqlite.SQLiteConstraintException e) {
                callback.error(new RotLiteException("SQLiteConstraintException " + e.getMessage(), e.hashCode()));
            } catch (android.database.sqlite.SQLiteException e) {
                callback.error(new RotLiteException("SQLiteException " + e.getMessage(), e.hashCode()));
            } catch (Exception e) {
                callback.error(new RotLiteException("Exception " + e.getMessage(), e.hashCode()));
            }
        }else if (this.from == RotLiteConsts.FROM_WEB){

            try {

                getData(url, jsonObject(), new Callback() {

                    @Override
                    public void onFailure(Request request, IOException e) {
                        Log.e(TAG, "Failure request: " + e.getMessage());
                    }

                    @Override
                    public void onResponse(Response response) throws IOException {

                        if (response.isSuccessful()) {
                            String responseStr = response.body().string();

                            if (activityLogs) Log.v(TAG, "Response: " + responseStr);

                            try {

                                JsonParser jsonParser = new JsonParser();
                                final JsonObject res = ((JsonObject) jsonParser.parse(responseStr)).getAsJsonObject();
                                JsonElement status = res.get("status");
                                //final JsonObject clinic = res.getAsJsonArray("clinic");
                                //final JsonObject user = res.getAsJsonObject("user");

                                if (status.getAsString().equals("SUCCESS")) {

                                    JsonArray data = res.getAsJsonArray("data");

                                    if (data.size() >= 2) {

                                        for (int i = 0; i < data.size(); i++) {

                                            try {

                                                RotLiteObject obj = new RotLiteObject();
                                                obj.setTbName(name);

                                                JsonObject object = data.get(i).getAsJsonObject();

                                                for(Map.Entry<String, JsonElement> entry : object.entrySet()) {

                                                    if (activityLogs) Log.v(TAG, entry.getKey() + ": " + entry.getValue());

                                                    try {
                                                        obj.put(entry.getKey(), entry.getValue().getAsString());
                                                    }catch (Exception e1) {
                                                        try {
                                                            obj.put(entry.getKey(), entry.getValue().getAsInt());
                                                        }catch (Exception e2) {
                                                            try {
                                                                obj.put(entry.getKey(), entry.getValue().getAsDouble());
                                                            }catch (Exception e3) {
                                                                try {
                                                                    obj.put(entry.getKey(), entry.getValue().getAsLong());
                                                                }catch (Exception e4) {
                                                                    try {
                                                                        obj.put(entry.getKey(), entry.getValue().getAsBoolean());
                                                                    }catch (Exception e5) {
                                                                        try {
                                                                            entry.getValue().getAsJsonNull();
                                                                        }catch (Exception e6) {
                                                                            callback.error(new RotLiteException("Dado não suportado: " + entry.getKey()));
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }

                                                }

                                                list.add(obj);

                                            }catch(IllegalStateException e) {
                                                JsonArray arr = data.get(i).getAsJsonArray();

                                                for (int z = 0; z < arr.size(); z++) {

                                                    RotLiteObject obj = new RotLiteObject();
                                                    obj.setTbName(name);

                                                    JsonObject object = arr.get(z).getAsJsonObject();

                                                    for(Map.Entry<String, JsonElement> entry : object.entrySet()) {

                                                        try {
                                                            obj.put(entry.getKey(), entry.getValue().getAsString());
                                                        }catch (Exception e1) {
                                                            try {
                                                                obj.put(entry.getKey(), entry.getValue().getAsInt());
                                                            }catch (Exception e2) {
                                                                try {
                                                                    obj.put(entry.getKey(), entry.getValue().getAsDouble());
                                                                }catch (Exception e3) {
                                                                    try {
                                                                        obj.put(entry.getKey(), entry.getValue().getAsLong());
                                                                    }catch (Exception e4) {
                                                                        try {
                                                                            obj.put(entry.getKey(), entry.getValue().getAsBoolean());
                                                                        }catch (Exception e5) {
                                                                            try {
                                                                                entry.getValue().getAsJsonNull();
                                                                            }catch (Exception e6) {
                                                                                callback.error(new RotLiteException("Dado não suportado: " + entry.getKey()));
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }

                                                    }

                                                    list.add(obj);

                                                }

                                            }

                                        }

                                        callback.done(list);

                                    }else {

                                        callback.error(new RotLiteException("Erro desconhecido"));

                                    }

                                }else if (status.getAsString().equals("ERROR")) {

                                    String message = res.get("message").getAsString();
                                    callback.error(new RotLiteException(message, Integer.parseInt(res.get("code").getAsString())));

                                }else{

                                    callback.error(new RotLiteException("Server error", RotLiteConsts.SERVER_ERROR));

                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                callback.error(new RotLiteException("Error: " + e.getMessage(), e.hashCode()));
                            }

                        } else {
                            Log.e(TAG, "Response error: " + response.message());
                            callback.error(new RotLiteException("Response error: " + response.message(), RotLiteConsts.RESPONSE_ERROR));
                        }
                    }

                });

            }catch(Exception e) {
                e.printStackTrace();
            }

        }

    }

    @Override
    public void find(String where, RotLiteCallback callback) {

    }

    @Override
    public void find(String select, String where, RotLiteCallback callback) {

    }

    @Override
    public void limit(int max) {
        limitMax = max;
        limit = " LIMIT " + max;
    }

    @Override
    public void limit(int min, int max) {
        limitMin = min;
        limitMax = max;
        limit = " LIMIT " + min + ", " + max;
    }

    @Override
    public JSONObject jsonObject() {

        JSONObject json = new JSONObject();
        if (content == null) return null;

        Set<String> keys = content.keySet();

        for (String key : keys) {

            try {

                String type = dataType.get(key);

                if (type.equals(RotLiteConsts.DATA_TYPE_STRING)) {
                    String v = content.getAsString(key);
                    json.put(key, v);
                }else if (type.equals(RotLiteConsts.DATA_TYPE_INTEGER)) {
                    int v = content.getAsInteger(key);
                    json.put(key, v);
                }else if (type.equals(RotLiteConsts.DATA_TYPE_DOUBLE)) {
                    double v = content.getAsDouble(key);
                    json.put(key, v);
                }else if (type.equals(RotLiteConsts.DATA_TYPE_LONG)) {
                    long v = content.getAsLong(key);
                    json.put(key, v);
                }else if (type.equals(RotLiteConsts.DATA_TYPE_BOOLEAN)) {
                    boolean v = content.getAsBoolean(key);
                    json.put(key, v);
                }

            }catch(JSONException e) {
                Log.e(TAG, "Get value error: " + e.getMessage());
            }

        }

        return json;
    }

    @Override
    public String jsonString() {

        JSONObject json = jsonObject();
        String jstring = json.toString();

        return jstring;
    }

    @Override
    public void getById(int id) {

        where = where + " AND _id =" + id;
        _id = id;

    }

    @Override
    public String getExecutedQuery() {
        return query;
    }

    @Override
    public void order(String order) {
        this.order = " ORDER BY " + order;
    }

    @Override
    public boolean delete() {
        method = RotLiteConsts.METHOD_DELETE;
        if (_id > 0) {
            if (activityLogs) Log.v(TAG_ACTIVITY, "Data deleted by id '" + _id + "'");
            return db.delete(name, "_id=" + _id, null) > 0;
        }else if (!where.equals("1 = 1")){
            if (activityLogs) Log.v(TAG_ACTIVITY, "Data deleted by query '" + where + "'");
            return db.delete(name, where, null) > 0;
        }else{
            if (activityLogs) Log.e(TAG_ACTIVITY, "Error deleting data");
            return false;
        }
    }

    @Override
    public boolean update() {
        method = RotLiteConsts.METHOD_UPDATE;
        if (_id > 0) {
            if (activityLogs) Log.v(TAG_ACTIVITY, "Data updated by id '" + _id + "'");
            return db.update(name, content, "_id=" + _id, null) > 0;
        }else if (!where.equals("1 = 1")){
            if (activityLogs) Log.v(TAG_ACTIVITY, "Data updated by query '" + where + "'");
            return db.update(name, content, where, null) > 0;
        }else{
            if (activityLogs) Log.v(TAG_ACTIVITY, "Error updating data");
            return false;
        }
    }

    @Override
    public void showActivityLogs(boolean activity) {
        activityLogs = activity;
    }

    @Override
    public void fromLocal() {
        this.from = RotLiteConsts.FROM_LOCAL;
    }

    @Override
    public void fromWeb() {
        this.from = RotLiteConsts.FROM_WEB;
    }

    @Override
    public void setEndpoint(String name) {
        this.url = this.url + name;
    }

    @Override
    public String getEndpoint() {
        return this.endpoint;
    }

    @Override
    public void getObject() throws RotLiteException {

        if (this.from == RotLiteConsts.FROM_LOCAL) {

            String sql = "SELECT * FROM " + name + " WHERE " + where;

            query = sql;

            try {
                Cursor cursor = db.rawQuery(query, null);
                if (cursor.moveToFirst()) {

                    String[] cols = cursor.getColumnNames();

                    for (int i = 0; i < cols.length; i++) {

                        this.put(cols[i], cursor.getString(i));

                    }

                }

                if (cursor != null && !cursor.isClosed()) {
                    cursor.close();
                }

            } catch (SQLiteDatabaseCorruptException e) {
                new RotLiteException("SQLiteDatabaseCorruptException " + e.getMessage().toString() + "", e.hashCode());
            } catch (android.database.sqlite.SQLiteConstraintException e) {
                new RotLiteException("SQLiteConstraintException " + e.getMessage(), e.hashCode());
            } catch (android.database.sqlite.SQLiteException e) {
                new RotLiteException("SQLiteException " + e.getMessage(), e.hashCode());
            } catch (Exception e) {
                new RotLiteException("Exception " + e.getMessage(), e.hashCode());
            }

        }

    }

    private Call saveData(String url, String json, Callback callback) throws IOException {
        RequestBody body = RequestBody.create(JSON, json);

        Request.Builder builder = new Request.Builder();
        builder.url(url);

        if (method == RotLiteConsts.METHOD_INSERT) {
            builder.post(body);
        }else if (method == RotLiteConsts.METHOD_UPDATE) {
            builder.put(body);
        }else if (method == RotLiteConsts.METHOD_DELETE) {
            builder.delete(body);
        }

        Request request = builder.build();
        Call call = client.newCall(request);
        call.enqueue(callback);
        return call;
    }

    private Call saveData(String url, JSONObject object, Callback callback) throws IOException {

        FormEncodingBuilder form = new FormEncodingBuilder();
        JSONArray names = object.names();

        for (int i = 0; i < object.length(); i++) {

            String name = "";

            try {

                name = names.getString(i);

                try {

                    form.add(name, object.getString(name));

                }catch(Exception e) {

                    try {

                        form.add(name, String.valueOf(object.getInt(name)));

                    }catch(Exception e2) {

                        try {
                            form.add(name, String.valueOf(object.getDouble(name)));
                        }catch(Exception e3) {
                            try {
                                form.add(name, String.valueOf(object.getLong(name)));
                            }catch(Exception e4) {

                                try {
                                    form.add(name, String.valueOf(object.getBoolean(name)));
                                }catch(Exception e5) {
                                    Log.e(TAG, "Get value error: " + e5.getMessage());
                                    e5.printStackTrace();
                                }

                            }
                        }

                    }

                }

            } catch (JSONException e) {
                Log.e(TAG, "JSONException: " + e.getMessage());
                e.printStackTrace();
            }

        }

        RequestBody formBody = form.build();

        Request.Builder builder = new Request.Builder();
        builder.url(url);

        if (method == RotLiteConsts.METHOD_INSERT) {
            builder.post(formBody);
        }else if (method == RotLiteConsts.METHOD_UPDATE) {
            builder.put(formBody);
        }else if (method == RotLiteConsts.METHOD_DELETE) {
            builder.delete(formBody);
        }

        Request request = builder.build();

        Call call = client.newCall(request);
        call.enqueue(callback);
        return call;
    }

    private Call getData(String url, JSONObject object, Callback callback) throws IOException {

        FormEncodingBuilder form = new FormEncodingBuilder();
        JSONArray names = object.names();

        url = url + "?";

        for (int i = 0; i < object.length(); i++) {

            String name = "";

            try {

                name = names.getString(i);

                try {

                    url = url + name + "=" + URLEncoder.encode(object.getString(name), "UTF-8") + "&";

                }catch(Exception e) {

                    try {

                        url = url + name + "=" + URLEncoder.encode(String.valueOf(object.getInt(name)), "UTF-8") + "&";

                    }catch(Exception e2) {

                        try {
                            url = url + name + "=" + URLEncoder.encode(String.valueOf(object.getDouble(name)), "UTF-8") + "&";
                        }catch(Exception e3) {
                            try {
                                url = url + name + "=" + URLEncoder.encode(String.valueOf(object.getLong(name)), "UTF-8") + "&";
                            }catch(Exception e4) {

                                try {
                                    url = url + name + "=" + URLEncoder.encode(String.valueOf(object.getBoolean(name)), "UTF-8") + "&";
                                }catch(Exception e5) {
                                    Log.e(TAG, "Get value error: " + e5.getMessage());
                                    e5.printStackTrace();
                                }

                            }
                        }

                    }

                }

            } catch (JSONException e) {
                Log.e(TAG, "JSONException: " + e.getMessage());
                e.printStackTrace();
            }

        }

        url = url.substring(0, url.length()-1);

        Request.Builder builder = new Request.Builder();
        builder.url(url);

        //Log.v(TAG, "url: " + url);

        Request request = builder.build();

        Call call = client.newCall(request);
        call.enqueue(callback);
        return call;
    }

    private void putLastUpdate() {
        final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        final Date date = new Date();
        this.put("rotlite_last_update", dateFormat.format(date));
    }

}