package example.rotlite.rotlite;

/**
 * Created by claudio on 09/08/15.
 */
public class RotLiteConsts {

    public static String ROTLITE_ERROR_HAS_NO_COLUMN = "has no column named";
    public static String ROTLITE_ERROR_NO_SUCH_TABLE = "no such table";
    public static int METHOD_INSERT = 0;
    public static int METHOD_UPDATE = 1;
    public static int METHOD_DELETE = 2;
    public static int FROM_LOCAL = 0;
    public static int FROM_WEB = 1;
    public static int RESPONSE_ERROR = 900;
    public static int SERVER_ERROR = 1000;

    public static String DATA_TYPE_STRING = "string";
    public static String DATA_TYPE_DOUBLE = "double";
    public static String DATA_TYPE_INTEGER = "int";
    public static String DATA_TYPE_LONG = "long";
    public static String DATA_TYPE_BOOLEAN = "boolean";

}
