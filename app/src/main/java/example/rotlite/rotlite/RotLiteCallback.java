package example.rotlite.rotlite;

import java.util.List;

/**
 * Created by claudio on 12/08/15.
 */
public interface RotLiteCallback<T> {
    void done(List<RotLiteObject> list);
    void error(RotLiteException e);
}
