package example.rotlite.rotlite;

import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;

import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by claudio on 08/08/15.
 */
public interface RotLiteInterface {

    void setTbName(String name);
    String getTbName();
    void setId(int id);
    long getId();
    void put(String key, String value);
    void put(String key, double value);
    void put(String key, int value);
    void put(String key, long value);
    void put(String key, boolean value);
    String getString(String key);
    double getDouble(String key);
    int getInt(String key);
    long getLong(String key);
    boolean getBoolean(String key);
    void saveLocal() throws Exception;
    void saveWeb(Callback callback) throws Exception;
    void save() throws Exception;
    boolean tbExists(String tbname);
    boolean createTable(String tbname);
    void where(String where);
    void find(RotLiteCallback callback);
    void find(String where,RotLiteCallback callback);
    void find(String select, String where, RotLiteCallback callback);
    void limit(int max);
    void limit(int min, int max);
    JSONObject jsonObject();
    String jsonString();
    void getById(int id);
    String getExecutedQuery();
    void order(String order);
    boolean delete();
    boolean update();
    void showActivityLogs(boolean activity);
    void fromLocal();
    void fromWeb();
    void setEndpoint(String name);
    String getEndpoint();
    void getObject() throws RotLiteException;

}
