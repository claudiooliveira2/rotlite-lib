package example.rotlite.rotlite;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.util.Log;

/**
 * Created by claudio on 12/09/15.
 */
public class RotLite {

    private static String TAG = "RotLite";
    private static RotLite instance;

    public static RotLite getInstance() {
        if (instance == null) instance = new RotLite();
        return instance;
    }

    public static void start(Context context) {

        if (!serviceRunning(context)) {
            context.startService(new Intent(context, RotLiteService.class));
            Log.v(TAG, "Starting service");
        }else{
            Log.v(TAG, "Service is running");
        }

    }

    private static boolean serviceRunning(Context context) {
        Class<?> serviceClass = RotLiteService.class;
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        return manager.getActiveNetworkInfo() != null &&
                manager.getActiveNetworkInfo().isConnectedOrConnecting();
    }

}
