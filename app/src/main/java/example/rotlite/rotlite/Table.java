package example.rotlite.rotlite;

/**
 * Created by claudio on 12/08/15.
 */
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE) //on class level
public @interface Table {

    String name() default "";
    String endpoint() default "";
    boolean autosync() default false;

}